<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';

    protected $fillable = [
        'bil_amount',
        'btc_amount',
        'usd_amount',
        'tx_hash',
        'user_id',
        'message',
        'notice',
        'bilAddress',
        'btcAddress',
    ];


    public function OrderAssigned()
    {
        return $this->belongsTo('App\Models\User','user_id')->orderBy('updated_at','desc');
    }
}
