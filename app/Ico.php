<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ico extends Model
{
    protected $table = 'next_ico_date';



    protected $fillable = [
        'available',
        'total',
        'sold',
        'date',
        'last_sold_at',
        'from',
        'to',
        'from_timestamp',
        'to_timestamp',
        'price',
    ];


}
