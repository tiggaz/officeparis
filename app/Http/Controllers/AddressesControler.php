<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Addresses as Addresses;

class AddressesControler extends Controller
{
    public function index()
    {
        $addresses = Addresses::orderby('id', 'desc')->get();
        $data = ['addresses' => $addresses];
        return view('pages.admin.addresses')->with($data);
    }

}
