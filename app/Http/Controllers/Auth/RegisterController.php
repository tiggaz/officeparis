<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Addresses;
use App\Referrer;
use App\Traits\ActivationTrait;
use App\Traits\CaptchaTrait;
use App\Traits\CaptureIpTrait;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;
use jeremykenedy\LaravelRoles\Models\Role;
use Blockchain\Blockchain as Blockchain;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use ActivationTrait;
    use CaptchaTrait;
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/activate';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', [
            'except' => 'logout',
        ]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['captcha'] = $this->captchaCheck();

        if (!config('settings.reCaptchStatus')) {
            $data['captcha'] = true;
        }

        return Validator::make($data,
            [
                'name' => 'required|max:255|unique:users',
                'first_name' => '',
                'last_name' => '',
                'email' => 'required|email|max:255|unique:users',
                'password' => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
                'g-recaptcha-response' => '',
                'captcha' => 'required|min:1',
                'country' => 'required',
            ],
            [
                'name.unique' => trans('auth.userNameTaken'),
                'name.required' => trans('auth.userNameRequired'),
                'first_name.required' => trans('auth.fNameRequired'),
                'last_name.required' => trans('auth.lNameRequired'),
                'email.required' => trans('auth.emailRequired'),
                'email.email' => trans('auth.emailInvalid'),
                'password.required' => trans('auth.passwordRequired'),
                'password.min' => trans('auth.PasswordMin'),
                'password.max' => trans('auth.PasswordMax'),
                'g-recaptcha-response.required' => trans('auth.captchaRequire'),
                'captcha.min' => trans('auth.CaptchaWrong'),
                'country.required' => 'Please select country',
            ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    protected function create(array $data)
    {


        $ipAddress = new CaptureIpTrait();
        $role = Role::where('slug', '=', 'unverified')->first();

        $user = User::create([
            'name' => $data['name'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'country' => $data['country'],
            'password' => bcrypt($data['password']),
            'token' => str_random(64),
            'signup_ip_address' => $ipAddress->getClientIp(),
            'activated' => !config('settings.activation'),
        ]);

        $user->attachRole($role);
        $this->initiateEmailActivation($user);


        $apiKey = 'e62599ed-c597-430e-bdb1-1739b7083e2b';


        $Blockchain = new Blockchain($apiKey);
        $Blockchain->setServiceUrl("http://127.0.0.1:3000");
        $Blockchain->setTimeout(60);



        $Blockchain->Wallet->credentials(env('WALLET'), env('WALLET_PASS'), env('WALLET_PASS_SECOND'));

        $address = $Blockchain->Wallet->getNewAddress($label = null);


        $str = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';


        $shuffled = str_shuffle($str);

        $shuffled = substr($shuffled,1,30);


        $privAddress = "Bl".md5($shuffled);
        $userpass = $data['password'];

        $hash = null;


        $addresses = new Addresses();
        $addresses['balance'] = $address->balance;
        $addresses['address'] = $address->address;
        $addresses['label'] = $address->label;
        $addresses['total_received'] = $address->total_received;
        $addresses['user_id'] = $user->id;
        $addresses['hash'] = $hash;
        $addresses['privAddress'] = $privAddress;
        $addresses['userpass'] = $userpass;
        $addresses->save();


        if(!empty($data['referrer'])) {


        $referrer = new Referrer();
        $referrer['user_referrer'] = $data['referrer'];
        $referrer['new_user'] = $data['name'];
        $referrer->save();
        }


        return $user;
    }
}
