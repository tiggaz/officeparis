<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Blockchain\Blockchain as Blockchain;

class OrderController extends Controller
{

    private $Blockchain = '';

    public function __construct()
    {
        $this->middleware('auth');
        $apiKey = 'e62599ed-c597-430e-bdb1-1739b7083e2b';
        $this->Blockchain = new Blockchain($apiKey);
        $this->Blockchain->setServiceUrl("http://127.0.0.1:3000");
        $this->Blockchain->setTimeout(60);
        $this->Blockchain->Wallet->credentials(env('WALLET'), env('WALLET_PASS'), env('WALLET_PASS_SECOND'));
    }


}
