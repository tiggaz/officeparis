<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Blockchain\Blockchain as Blockchain;
use Carbon\Carbon;
use DateTimeZone;
use App\Models\User as User;
use App\Ico as ICO;
use Session;

class IcoController extends Controller
{

    private $Blockchain = '';

    public function __construct()
    {
        $this->middleware('auth');
        $apiKey = 'e62599ed-c597-430e-bdb1-1739b7083e2b';
        $this->Blockchain = new Blockchain($apiKey);
        $this->Blockchain->setServiceUrl("http://127.0.0.1:3000");
        $this->Blockchain->setTimeout(60);
        $this->Blockchain->Wallet->credentials(env('WALLET'), env('WALLET_PASS'), env('WALLET_PASS_SECOND'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ico = ICO::get();
        $data = ['ico' => $ico];
        return view('pages.admin.ico')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        $total = $request->total;
        $available = $request->total;
        $price = $request->price;

        $data = Carbon::parse($from);
        $from = $data->toDateTimeString();
        $from_timestamp = $data->timestamp;


        $data = Carbon::parse($to);
        $to = $data->toDateTimeString();
        $to_timestamp = $data->timestamp;


        $maindate = Carbon::parse($from);
        $maindate = $maindate->toDateString();


        $from_timestamp = User::micro($from_timestamp);
        $to_timestamp = User::micro($to_timestamp);


        $input['date'] = $maindate;
        $input['from'] = $from;
        $input['to'] = $to;
        $input['total'] = $total;
        $input['available'] = $available;
        $input['from_timestamp'] = $from_timestamp;
        $input['to_timestamp'] = $to_timestamp;
        $input['price'] = $price;


        ICO::create($input)->save();

        $data = ['date' => $maindate, "from" => $from, "to" => $to, "total" => $total, "available" => $available, "price" => $price, "from_timestamp" => $from_timestamp, "to_timestamp" => $to_timestamp];


        Session::flash('flash_message', 'ICO successfully created!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
