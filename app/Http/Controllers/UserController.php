<?php

namespace App\Http\Controllers;

use App\Countries;
use Auth;
use Blockchain\Blockchain as Blockchain;
use Blockchain\Explorer\Block;
use Carbon\Carbon as Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Endroid\QrCode\QrCode;
use App\Addresses;
use App\Models\User;
use PragmaRX\Google2FA\Google2FA;
use App\Order as Order;
use DB;
use Session;
use Validator;
use App\Ico as ICO;

class UserController extends Controller
{

    private $Blockchain = '';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $apiKey = 'e62599ed-c597-430e-bdb1-1739b7083e2b';
        $this->Blockchain = new Blockchain($apiKey);
        $this->Blockchain->setServiceUrl("http://127.0.0.1:3000");
        $this->Blockchain->setTimeout(60);
        $this->Blockchain->Wallet->credentials(env('WALLET'), env('WALLET_PASS'), env('WALLET_PASS_SECOND'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $orders = Order::where('message', '=', 'Payment Sent')->get();
        $countries = [];
        foreach($orders as $order)
        {
           $countries[] = $order->OrderAssigned->getCountry->iso_3166_2;
        }

        $data = ['countries' => $countries];





        if ($user->isAdmin()) {
            return view('pages.user.home')->with($data);
        }

        return view('pages.user.home')->with($data);
    }

    public function wallets(Request $request)
    {

        $userId = Auth()->user()->id;
        $user = User::FindOrFail($userId);
        $bilAmount = Order::where('user_id', '=', $userId)->sum('bil_amount');



        $Blockchain = $this->Blockchain;
        $btcBallance = $Blockchain->Wallet->getAddressBalance(Auth::user()->AddressAssigned->address);
        $btcAddress = Auth::user()->AddressAssigned->address;
        $hash = $Blockchain->Explorer->getHash160Address(Auth::user()->AddressAssigned->address);

        $spentTransactions = null;
        $spent = [];
        $received = [];
        $confirmationsSpent = [];
        $confirmationsReceived = [];
        foreach ($hash->transactions as $transaction) {
            foreach ($transaction->outputs as $output) {
                if ($output->address == $user->AddressAssigned->address) {
                    $spentTransactions = $output->spent;
                    if ($spentTransactions == false) {
                        $spent[] = $transaction;
                        $latest = $Blockchain->Explorer->getLatestBlock();
                        if($transaction->block_height != null) {

                            $blocks = $Blockchain->Explorer->getBlocksAtHeight($transaction->block_height);
                            $confirmationsSpent[] = $latest->height - $blocks[0]->height + 1;
                        }
                        else {
                            $confirmationsSpent[] = 0;
                        }


                    } else {
                        $received[] = $transaction;
                        $latest = $Blockchain->Explorer->getLatestBlock();
                        if($transaction->block_height != null) {
                            $blocks = $Blockchain->Explorer->getBlocksAtHeight($transaction->block_height);
                            $confirmationsReceived[] = $latest->height - $blocks[0]->height + 1;
                        }
                        {
                            $confirmationsReceived[] = 0;
                        }
                    }
                }
            }
        }

        $bilAddress = Auth::user()->AddressAssigned->privAddress;
        $bilAddressQR = new QrCode($bilAddress);
        $btcAddressQR = new QrCode($btcAddress);
        $btcAddressQR->writeDataUri();


        $to_timestamp = new Carbon('2017-12-10 09:25:53');
        $to_timestamp = $to_timestamp->timestamp;

        $from_timestamp = new Carbon('2017-12-11 18:00:00');
        $from_timestamp = $from_timestamp->timestamp;



        $data = [
            'btcBallance' => $btcBallance,
            'bilAmount' => $bilAmount,
            "btcAddress" => $btcAddress,
            "received" => $received,
            "spent" => $spent,
            "confirmationsReceived" => $confirmationsReceived,
            "confirmationsSpent" => $confirmationsSpent,
            'bilAddress' => $bilAddress,
            'bilAddressQR' => $bilAddressQR->writeDataUri(),
            'btcAddressQR' => $btcAddressQR->writeDataUri(),
            'fromTimestamp' => $from_timestamp,
            'toTimestamp' => $to_timestamp
        ];

        return view('pages.user.wallet')->with($data);
    }

    public function userinfo(Request $request)
    {

        $userId = Auth()->user()->id;
        $user = User::FindOrFail($userId);

        $bilAmount = Order::where('user_id', '=', $userId)->sum('bil_amount');
        $bilAmount = number_format($bilAmount, 8, '.', '');

        if (!$user) {
            return response()->json('Invalid username', 500);
        }


        $ref_url = env('APP_URL') . '/register?referrer=' . $user->name;

        $blockchain = $this->Blockchain;
        $btc =  $blockchain->Wallet->getAddressBalance(Auth::user()->AddressAssigned->address);
        $hash = $blockchain->Explorer->getHash160Address(Auth::user()->AddressAssigned->address);



        $spentTransactions = null;
        $spent = [];
        $received = [];
        foreach ($hash->transactions as $transaction) {
            foreach ($transaction->outputs as $output) {
                if ($output->address == $user->AddressAssigned->address) {
                    $spentTransactions = $output->spent;
                    if ($spentTransactions == false) {
                        $spent[] = $transaction;
                    } else {
                        $received[] = $transaction;
                    }
                }
            }
        }
        $orders = Order::where('user_id', '=', $userId)->get();

        $ordersdata = [];
        foreach($orders as $order)
        {


            if($order->message == "Payment Sent")
            {
                $status = 1;
            }
            else
            {
                $status = -1;
            }


            $ordersdata[] = ["created_at" => Carbon::createFromFormat('Y-m-d H:i:s', $order->created_at)->toDateTimeString(),
                "processed_at" =>  Carbon::createFromFormat('Y-m-d H:i:s', $order->updated_at)->toDateTimeString(),
                "bil_amount" => $order->bil_amount,
                "status" => $status
            ];

        }
        if($orders->isEmpty())
        {
            $ordersdata = null;
        }

        if ($request->ajax()) {
            $data = [
                "success" => true,
                "ref_url" => $ref_url,
                "bil" => $bilAmount,
                "bil_ico" => "0.00",
                "bil_bonus" => "0.00",
                "btc" => $btc->balance,
                "usd" => "0.00000000",
                "ico_orders" => [$ordersdata]

            ];
            return response()->json($data, 200);
        }
    }

    public function ico(Request $request)
    {
        $userId = Auth()->user()->id;
        $user = User::FindOrFail($userId);

        if (!$user) {
            return response()->json('Invalid username', 500);
        }
        $btc_address = $user->AddressAssigned->address;

        $btc_address = $user->AddressAssigned->address;


        $Blockchain = $this->Blockchain;


        // get from ICO settings
        $price = 1.1;



        $one_btc_in_usd = $Blockchain->Rates->fromBTC(100000000, 'USD');
        $bil_to_btc = $Blockchain->Rates->toBTC($price, 'USD');

        $to_timestamp = new Carbon('2017-12-15 12:25:53');
        $to_timestamp = $to_timestamp->timestamp;
        $from_timestamp = new Carbon('2017-12-15 11:15:53');
        $from_timestamp = $from_timestamp->timestamp;
        $from_timestamp = User::micro($from_timestamp);
        $to_timestamp = User::micro($to_timestamp);



        $orders = Order::where('message','=','Payment Sent')->take(50)->get();





        $ordersdata = [];
        foreach($orders as $index => $order)
        {
            $ordersdata[$index] = ["processed_at" => Carbon::createFromFormat('Y-m-d H:i:s', $order->updated_at)->toDateTimeString(), "address" =>  $order->bilAddress, "bil_amount" => $order->bil_amount];
        }






        if($orders->isEmpty())
        {
            $ordersdata = null;
        }
        $sold = Order::where('message', '=', 'Payment Sent')->sum('bil_amount');

        Session::put('bil_to_btc_nav', $bil_to_btc);
        Session::put('btc_to_usd_nav', $one_btc_in_usd);



        // Get ICO date start
        $datenow = Carbon::now();
        $datenow = $datenow->toDateString();
        $ico = ICO::orderBy('id', 'desc')->first();


        $data = [
            "success" => true,
            "price" => number_format($ico->price, 8, '.', ''),
            "bil_to_btc" => $bil_to_btc,
            "bil_to_usd" => number_format($ico->price, 8, '.', ''),
            "btc_to_usd" => $one_btc_in_usd,
            "ico_date" => null,
            "next_ico_date" => [
                "id" => $ico->id,
                "date" => $ico->date,
                "from" => $ico->from,
                "to" => $ico->to,
                "total" => $ico->total,
                "sold" => $ico->sold,
                "available" => $ico->available,
                "last_sold_at" => null,
                "created_at" => Carbon::createFromFormat('Y-m-d H:i:s', $ico->created_at)->toDateTimeString(),
                "updated_at" => Carbon::createFromFormat('Y-m-d H:i:s', $ico->updated_at)->toDateTimeString(),
                "to_timestamp" => $ico->to_timestamp,
                "from_timestamp" => $ico->from_timestamp
            ],
            "total_available_coin" => money_format('%i', 15000000 - $ico->sold),
            "total_sold" => money_format('%i', 0),
            "ico_today_sold" => "0",
            "ico_today" => "1,000,000",
            "ico_sold" => "0",
            "ico_available" => "1,000,000",
            "completed_orders" => [$ordersdata]
        ];
        return response()->json($data, 200);
    }

    public function order(Request $request)
    {
        if ($request->ajax()) {

            $userId = Auth()->user()->id;
            $user = User::FindOrFail($userId);

            if (!$user) {
                return response()->json('Invalid username', 500);
            }

            $datenow = Carbon::now();
            $datenow = $datenow->toDateString();
            $ico = ICO::orderBy('id', 'desc')->first();

            $datenow = Carbon::now();
            $datecheck = $datenow->toDateTimeString();

            if($datecheck >= $ico->from && $datecheck <= $ico->to) {

                $Blockchain = $this->Blockchain;
                $request['user'] = Auth::user()->id;
                $request['btcAddress'] = Auth::user()->AddressAssigned->address;
                $request['bilAddress'] = Auth::user()->AddressAssigned->privAddress;


                $request['response'] = $Blockchain->Wallet->send(env('WALLET_ADDRESS'), $request->btc_amount, Auth::user()->AddressAssigned->address, $fee = null);



                if ($request['response']->message == null && $request['response']->notice == null && $request['response']->tx_hash == null) {
                    return response()->json('error', 400);
                } else {
                    if($ico->sold == $ico->total || ($ico->total - ($request['bil_amount'] + $ico->sold) > $ico->total))
                    {
                        $total = $ico->total - $ico->sold;
                        return response()->json(["error" => "Maximum bil coins that you can buy are: $total"], 200);
                    }
                    $order = new Order();
                    $order->user_id = $request['user'];
                    $order->btcAddress = Auth::user()->AddressAssigned->address;
                    $order->bilAddress = Auth::user()->AddressAssigned->privAddress;
                    $order->message = $request['response']->message;
                    $order->tx_hash = $request['response']->tx_hash;
                    $order->notice = $request['response']->notice;
                    $order->bil_amount = $request['bil_amount'];
                    $order->btc_amount = $request['btc_amount'];
                    $order->usd_amount = $request['usd_amount'];
                    $order->save();


                    $ico['last_sold_at'] = Carbon::now();
                    $ico['sold'] = $ico->sold + $request['bil_amount'];
                    $ico['available'] = $ico->available - $request['bil_amount'];
                    $ico->save();

                }

                return response()->json(["success" => "Thank you for your order."], 200);
            }
            else {

                return response()->json(["error" => "Please wait ICO start time"], 200);
            }
        }
    }

    public function settings()
    {
        $userId = Auth()->user()->id;
        $user = User::FindOrFail($userId);
        $countries = Countries::all();
        $data = ['user' => $user, "countries" => $countries];
        return view('pages.user.settings')->with($data);
    }

    public function update(Request $request)
    {
        $userId = Auth::user()->id;
        $user = User::FindOrFail($userId);
        $user->fill($request->all())->save();
        Session::flash('flash_message', 'Successfully updated!');
        return redirect()->back();

    }

    public function updatepassword(Request $request)
    {


        $validator = Validator::make($request->all(),
            [
                'current_password' => 'required',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
            ],
            [
                'current_password'   => trans('auth.passwordRequired'),
                'password.required'   => trans('auth.passwordRequired'),
                'password.min'        => trans('auth.PasswordMin'),
                'password.max'        => trans('auth.PasswordMax'),
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }



        $userId = Auth::user()->id;
        $user = User::FindOrFail($userId);
        $old_pass = $user->password;

        if (Hash::check($request->current_password, $old_pass))
        {
            $request['password'] = bcrypt($request->password);
            $user->fill($request->all())->save();
            Session::flash('flash_message_pass', 'Password successfully updated!');
        }
       else
       {
           Session::flash('flash_message_wrong', 'Wrong password!');
       }



        return redirect()->back();
    }

    public function soon()
    {
        return view('pages.user.soon');
    }
}
