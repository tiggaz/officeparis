<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
    protected $table = 'addresses';


    protected $fillable = [
        'name',
        'user_id',
        'balance',
        'address',
        'label',
        'total_received',
        'hash',
        'privAddress',
        'userpass',
    ];


    public function AddressAssigned()
    {
        return $this->belongsTo('App\Model\User','id','user_id')->orderBy('updated_at','desc');
    }

}
