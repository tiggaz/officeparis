<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Referrer extends Model
{
    protected $table = 'referrer';

    protected $fillable = [
        'user_referrer',
        'new_user',
    ];
}
