<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->double('price');
            $table->double('bil_to_btc');
            $table->double('bil_to_usd');
            $table->double('btc_to_usd');
            $table->date('ico_date');
            $table->double('total_available_coin');
            $table->double('total_sold');
            $table->double('ico_today_sold');
            $table->double('ico_today');
            $table->double('ico_sold');
            $table->double('ico_available');
            $table->integer('completed_orders');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
