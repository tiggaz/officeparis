<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIcoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ico', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->date('from');
            $table->date('to');
            $table->double('total');
            $table->double('sold');
            $table->double('available');
            $table->date('last_sold_at')->nullable();
            $table->string('to_timestamp');
            $table->string('from_timestamp');
            $table->integer('settings_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ico');
    }
}
