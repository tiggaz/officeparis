<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNextIcoDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('next_ico_date', function (Blueprint $table) {
            $table->increments('id');
            $table->double('available');
            $table->double('total');
            $table->string('price');
            $table->double('sold')->nullable();
            $table->date('date');
            $table->date('last_sold_at')->nullable();
            $table->dateTime('from');
            $table->dateTime('to');
            $table->string('from_timestamp');
            $table->string('to_timestamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('next_ico_date');
    }
}
