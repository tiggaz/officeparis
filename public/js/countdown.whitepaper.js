(function($){
    $(document).ready(function() {
        var countdown =  $('.countdown-time-whitepaper');
        createTimeCicles();
        $(window).on('resize', windowSize);

        function windowSize(){
            countdown.TimeCircles().destroy();
            createTimeCicles();
            countdown.on('webkitAnimationEnd mozAnimationEnd oAnimationEnd animationEnd', function() {
                countdown.removeClass('animated bounceIn');
            });
        }

        // TimeCicles - Create and Options
        function createTimeCicles() {
            countdown.addClass('animated bounceIn');
            countdown.TimeCircles({
                bg_width: 1,
                fg_width: 0.04,
                circle_bg_color: '#ccc',
                time: {
                    Days: {color: '#212121'}
                    ,	   Hours: {color: '#212121'}
                    ,	 Minutes: {color: '#212121'}
                    ,	 Seconds: {color: '#212121'}
                }
            });
            countdown.on('webkitAnimationEnd mozAnimationEnd oAnimationEnd animationEnd', function() {
                countdown.removeClass('animated bounceIn');
            });
        }

    });
})(jQuery);