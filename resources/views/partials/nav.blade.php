<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <div id="mobile-menu">
                <div class="left-nav-toggle">
                    <a href="#">
                        <i class="stroke-hamburgermenu"></i>
                    </a>
                </div>
            </div>
            {{-- Branding Image --}}
            <a class="navbar-brand" href="{{ url('/home') }}">
                <img src="/images/logo.png" alt="{{ env('APP_NAME') }}" style="max-width: 60px; margin-left:40px;"/>
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="left-nav-toggle">
                <a href="">
                    <i class="stroke-hamburgermenu"></i>
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                {{-- Authentication Links --}}
                @if (Auth::guest())
                    <li><a href="{{ route('login') }}">{!! trans('titles.login') !!}</a></li>
                    <li><a href="{{ route('register') }}">{!! trans('titles.register') !!}</a></li>
                @else

                    <ul class="nav navbar-nav navbar-right hidden-sm">
                        <li><a href="#" class="f-s-14">1 BIL = BTC <span id="bil_to_btc_nav"></span></a></li>
                        <li><a href="#" class="f-s-14">1 BIL = USD <span id="bil_to_usd_nav"></span></a></li>
                        <li><a href="#" class="f-s-14">1 BTC = USD <span id="btc_to_usd_nav"></span></a></li>
                        <li><a href="#" class="f-s-14 base-font-color" style="color: gold; margin-right: 20px;">  {{ Auth::user()->name }}</a></li>
                        <li class="profil-link"></li>
                    </ul>
                @endif
            </ul>
        </div>

    </div>
</nav>



<!-- Navigation-->
<aside class="navigation">
    <nav>
        <ul class="nav luna-nav">
            <li class="nav-category">
                {{ env('APP_NAME') }}
            </li>

            <li {{ Request::is('home') ? 'class=active' : null }}>
                <a href="/home">ICO</a>

            </li>
            <li {{ Request::is('wallets') ? 'class=active' : null }}>
                <a href="/wallets">Wallets</a>
            </li>
            <li {{ Request::is('settings') ? 'class=active' : null }}>
                <a href="/settings">Settings</a>
            </li>
            <li {{ Request::is('security') ? 'class=active' : null }}>
                <a href="/security">Security (2FA)</a>
            </li>
            <li {{ Request::is('tools') ? 'class=active' : null }}>
                <a href="/tools">Affiliate Tools</a>
            </li>
            <li {{ Request::is('exchange') ? 'class=active' : null }}>
                <a href="/exchange">Exchange</a>
            </li>

            <li>
                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                    {!! trans('titles.logout') !!}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
            @role('admin')
            <li class="nav-category">
                Administator
            </li>
            <li>


                <a href="#uielements" data-toggle="collapse" aria-expanded="false">
                    Settings<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                </a>
                <ul id="uielements" class="nav nav-second collapse">
                            <li {{ Request::is('users', 'users/' . Auth::user()->id, 'users/' . Auth::user()->id . '/edit') ? 'class=active' : null }}>{!! HTML::link(url('/users'), Lang::get('titles.adminUserList')) !!}</li>
                            <li {{ Request::is('users/create') ? 'class=active' : null }}>{!! HTML::link(url('/users/create'), Lang::get('titles.adminNewUser')) !!}</li>
                            <li {{ Request::is('adminico') ? 'class=active' : null }}>{!! HTML::link(url('/adminico'), 'ICO Settings') !!}</li>
                            <li {{ Request::is('themes','themes/create') ? 'class=active' : null }}>{!! HTML::link(url('/themes'), Lang::get('titles.adminThemesList')) !!}</li>
                            <li {{ Request::is('logs') ? 'class=active' : null }}>{!! HTML::link(url('/logs'), Lang::get('titles.adminLogs')) !!}</li>
                            <li {{ Request::is('php') ? 'class=active' : null }}>{!! HTML::link(url('/php'), Lang::get('titles.adminPHP')) !!}</li>
                            <li {{ Request::is('routes') ? 'class=active' : null }}>{!! HTML::link(url('/routes'), Lang::get('titles.adminRoutes')) !!}</li>

                    @endrole
                </ul>
            </li>

        </ul>
    </nav>
</aside>
<!-- End navigation-->