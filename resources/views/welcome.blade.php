@extends('layouts.main')
@section('content')

    <div class="clearfix"></div>


    <!-- About US -->
    <div class="contain-wrapp padding-bottom-30 background-dark" id="about">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="section-heading">
                        <h3>ABOUT BILLIONARIE COIN – ( BIL )</h3>
                        <p>How does really this digital asset BILLIONAIRE COIN work?</p>
                        <i class="fa fa-user"></i>
                    </div>
                </div>
            </div>

            <p>Without relying on a central server, this digital asset is designed to operate as a unit of exchange.
                Open protocol is used by BILLIONAIRE COIN - (BIL) to facilitate secure payment transactions. The
                storage
                server is decentralized and distributed-divided into different servers. The various servers are run
                by
                each user that is connected to the network.
            </p>

            <p>BILLIONAIRE COIN itself was created based on the platform Bitcoin and that is why it is nearly
                identical
                to
                it. You can easily send BILLIONAIRE COIN anywhere in the world just in couple of seconds as long as
                the
                recipient is connected to the internet, same as its predecessor.

                All the transactions that have been made are recorded and displayed by the Blockchain Network.
                Despite
                the
                fact that you can anonymously set your identity, BILLIONAIRE COIN Blockchain Explorer monitors all
                your
                transactions.

                The transfer fee can be gradually reduced. Also, a higher fee can be set by you in the BILLIONAIRE
                COIN
                -
                Wallet in order to speed up the transaction.

                If the recipient is connected to the internet you have the ability to transfer BILLIONAIRE COIN
                anywhere.
                Why is BILLIONAIRE COIN – (BIL) valuable?

                BILLIONAIRE COIN will release only 30 million coins through the world. BILLIONAIRE COIN prices will
                rise
                by
                increasing demand and limited supply.

                When talking about the Investors, BILLIONAIRE COIN can also be used as a stacking program in the
                exchange market, and 40% daily income can be got from bot trading against altcoins with our unique
                innovative algorithm on exchange platform.

                But when we talk about the individuals, BILLIONAIRE COIN can also be traded on cryptocurrency
                exchanges.
                The
                profit itself can be made from price movements.
            </p>
            <h4>
                What is BILLIONAIRE COIN actually?
            </h4>
            <p>
                It is a digital currency similar to Bitcoin with P2P transaction which is based on an open source
                platform.
            </p>
            <p>
                It can also be used for a exclusive lending program on BILLIONAIRECOIN.CO
                Payment of trading fees in the exchange market are the other things for which BILLIONAIRE COIN can
                be
                also
                used.
            </p>
            <h4>
                Can BILLIONAIRE COIN be mined?
            </h4>
            <p>
                Of course, BILLIONAIRE COIN can be mined with a starting date on July 30, 2018.
            </p>

            <h4>Coin Properties</h4>
            <ul>
                <li>Name: Billionaire Coin</li>
                <li>Code: BIL</li>
                <li>Algorithm Script</li>
                <li>Type Pow</li>
                <li>Block Reward 25coins (BIL)</li>
                <li>Block Space 3 min</li>
                <li>Total Supply 30.000.000 Coins</li>
                <li>ICO 15.000.000 Coins</li>
                <li>Bounty 1.000.000 Coins</li>
                <li>Mining 14.000.000 Coins</li>
            </ul>
        </div>
    </div>


    <!-- END About US -->




    <!-- ICO-->
    <div class="contain-wrapp padding-bottom-30 background-dark" id="ico">

        <div class="otherbg">
            <div class="container">
                <div class="row ">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="section-heading">
                            <h3>What does (ICO) Initial Coin Offering represents?</h3>
                            <p>How does really this digital asset BILLIONAIRE COIN work?</p>
                            <i class="fa fa-user"></i>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <p>
                            We offer the digital asset BILLIONAIRE COIN via the ICO by issuing BILLIONAIRE COIN to the
                            ones
                            who want
                            to own them before it is released in the open market.
                        </p>
                        <p>
                            The goal of the Initial Coin Offering is the following:
                        </p>
                        <p>
                            - It is not an easy task to build a cryptocurrency coin and exchange cryptocurrency. That is
                            why
                            we need
                            additional funding for the reason that programmers, servers, and advertise can be recruited.
                            The Price of the ICO
                        </p>

                        <h5>
                            Billionaire Coin - BIL<br/>

                        </h5>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="gradient table table-bordered">
                        <thead>
                        <tr>

                            <th>FROM</th>
                            <th>TO</th>
                            <th>PRICE</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>0</td>
                            <td>1,000,000</td>
                            <td>$ 0.70</td>
                        </tr>
                        <tr>
                            <td>1,000,000</td>
                            <td>2,000,000</td>
                            <td>$ 0.75</td>
                        </tr>
                        <tr>
                            <td>2,000,000</td>
                            <td>3,000,000</td>
                            <td>$ 0.80</td>
                        </tr>
                        <tr>
                            <td>3,000,000</td>
                            <td>4,000,000</td>
                            <td>$ 0.85</td>
                        </tr>
                        <tr>
                            <td>4,000,000</td>
                            <td>5,000,000</td>
                            <td>$ 0.90</td>
                        </tr>
                        <tr>
                            <td>5,000,000</td>
                            <td>6,000,000</td>
                            <td>$ 0.95</td>
                        </tr>
                        <tr>
                            <td>6,000,000</td>
                            <td>7,000,000</td>
                            <td>$ 1.00</td>
                        </tr>
                        <tr>
                            <td>7,000,000</td>
                            <td>8,000,000</td>
                            <td>$ 1.05</td>
                        </tr>
                        <tr>
                            <td>8,000,000</td>
                            <td>9,000,000</td>
                            <td>$ 1.10</td>
                        </tr>
                        <tr>
                            <td>9,000,000</td>
                            <td>10,000,000</td>
                            <td>$ 1.15</td>
                        </tr>
                        <tr>
                            <td>10,000,000</td>
                            <td>11,000,000</td>
                            <td>$ 1.20</td>
                        </tr>
                        <tr>
                            <td>11,000,000</td>
                            <td>12,000,000</td>
                            <td>$ 1.25</td>
                        </tr>
                        <tr>
                            <td>12,000,000</td>
                            <td>13,000,000</td>
                            <td>$ 1.30</td>
                        </tr>
                        <tr>
                            <td>13,000,000</td>
                            <td>14,000,000</td>
                            <td>$ 1.35</td>
                        </tr>
                        <tr>
                            <td>14,000,000</td>
                            <td>15,000,000</td>
                            <td>$ 1.40</td>
                        </tr>
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
    </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="section-heading">
                    <h3>ICO PRICE</h3>
                    <i class="fa fa-dollar"></i>
                </div>
            </div>

            <div class="col-md-12">


                <div id="progress-bar">
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 0.70] </span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 0.75] </span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 0.80] </span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 0.85]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 0.90]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 0.95]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.00]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.05]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.10]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.15]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.20]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.25]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.30]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.35]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                    <div class="progress progress-striped progress-orange active">
                        <span class="value-progress">[$ 1.40]</span>
                        <div class="progress-bar" role="progressbar" data-value-progress="0">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End ICO -->



    <!-- RoadMap -->
    <section class="main-container background-dark-1" id="roadmap">

        <div class="container bgleft">
            <div class="bgright">
                <div class="row">

                    <!-- main start -->
                    <!-- ================ -->
                    <div class="main col-12">

                        <!-- page-title start -->
                        <!-- ================ -->
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-heading">
                                <h3>ROADMAP</h3>
                                <i class="fa fa-road"></i>
                            </div>
                        </div>
                        <div class="separator-2"></div>
                        <!-- page-title end -->

                        <!-- timeline grid start -->
                        <!-- ================ -->
                        <div class="timeline clearfix">


                            <div class="timeline-item">

                                <i class="fa fa-code fa-2x pull-right"></i>

                                <h5 class="pull-right">NOVEMBER 2017</h5>
                                <div class="pull-right-info">
                                    <h6>DEVELOPING</h6>
                                    <ul class="list-icons">
                                        <li>BILLIONAIRE COIN CONCEPT INITIATION <i class="fa fa-check-square"></i></li>
                                        <li>COIN BUILDING <i class="fa fa-check-square"></i></li>
                                        <li>WEB WALLET <i class="fa fa-check-square"></i></li>
                                        <li>WEBSITE DEVELOPMENT <i class="fa fa-check-square"></i></li>

                                    </ul>
                                </div>


                            </div>

                            <div class="timeline-item pull-right">

                                <i class="fa fa-database fa-2x float-left"></i>
                                <h5 class="float-left">MID DECEMBER 2017</h5>
                                <p class="float-left">
                                <h6 style="clear: both;">HEAVY-BILLIONAIRE DISTRIBUTION</h6>
                                <ul class="list-icons">
                                    <li><i class="fa fa-check-square"></i> ICO (INITIAL COIN OFFERING)</li>
                                    <li><i class="fa fa-check-square"></i> LAUNCHING AND MARKETING COMMUNICATION</li>
                                    <li><i class="fa fa-check-square"></i> DDOS ATTACK PROTECTION</li>
                                </ul>

                            </div>

                            <div class="timeline-item">
                                <div class="blue-bg">
                                    <i class="fa fa-rocket fa-2x pull-right"></i>
                                </div>
                                <h5 class="pull-right">MID JANUARY 2017</h5>
                                <div class="pull-right-info">
                                    <h6>DISTRIBUTION AND INVESTMENT</h6>
                                     
                                    <ul class="list-icons">
                                        <li>POW AND POS <i class="fa fa-check-square"></i></li>
                                        <li>LENDING PROGRAM <i class="fa fa-check-square"></i></li>
                                        <li>AFFILIATE PROGRAM <i class="fa fa-check-square"></i></li>
                                        <li>INTERNAL EXCHANGE MARKET <i class="fa fa-check-square"></i></li>
                                    </ul>
                                </div>
                            </div>

                            <div class="timeline-item pull-right">
                                <i class="fa fa-road fa-2x float-left"></i>
                                <h5 class="float-left">MID FEBRUARY 2018</h5>
                                <p class="float-left">
                                <h6 style="clear: both;">GROWING</h6>
                                 
                                <ul class="list-icons">
                                    <li><i class="fa fa-check-square"></i> LISTING IN COINMARKETCAP</li>
                                    <li><i class="fa fa-check-square"></i> TARGET PRICE ACHIEVEMENT 30 USD/BILLIONAIRE
                                        COIN
                                    </li>
                                    <li><i class="fa fa-check-square"></i> AFFILIATE PROGRAM</li>
                                    <li><i class="fa fa-check-square"></i> ANDROID/IOS/WINDOWS MOBILE PLATFORM
                                        DEVELOPMENT
                                    </li>
                                </ul>
                            </div>

                            <div class="timeline-item">
                                <i class="fa fa-send-o fa-2x pull-right"></i>
                                <h5 class="pull-right">MID MARCH 2018</h5>
                                <div class="pull-right-info">
                                    <h6>MARKETING</h6>
                                     
                                    <ul class="list-icons">
                                        <li>CAMPAIGN <i class="fa fa-check-square"></i></li>
                                        <li>LIST IN EXCHANGE MARKETS <i class="fa fa-check-square"></i></li>
                                        <li>IMPROVE WEBSITE <i class="fa fa-check-square"></i></li>
                                        <li>BOT DEPLOYMENT BTC/BIL AGAINST ALTCOINS <i class="fa fa-check-square"></i>
                                        </li>
                                        <li>BOT DEPLOYMENT BTC/BIL AGAINST USD DOLLAR <i class="fa fa-check-square"></i>
                                        </li>
                                        <li>TARGET PRICE ACHIEVEMENT 100 USD/BILLIONAIRE COIN</li>
                                    </ul>
                                </div>
                            </div>


                            <div class="timeline-item pull-right">
                                <i class="fa fa-dot-circle-o fa-2x float-left"></i>
                                <h5 class="float-left">END OF YEAR 2018</h5>
                                <p class="float-left">

                                    TARGET PRICE 350 USD / BILLIONAIRE COIN<br/>
                                    DECENTRALIZATION TO OVER 3 BILLION MOBILE DEVICES
                                </p>
                            </div>


                        </div>


                    </div>


                </div>
            </div>
        </div>
    </section>
    <!-- END RoadMap -->



            <!-- Lending or saving -->
            <div class="contain-wrapp padding-bottom-30 background-dark" id="landing">

                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-heading">
                                <h3>LENDING OR SAVING</h3>
                                <p>This is a very unique lending (BIL Algorithm Code or saving program)</p>
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                    </div>

                    <p>
                        You can withdraw Billionaire Coin or renew your contract if you want when the lending contract
                        is
                        completed. All the coins in your wallet can also be lend by you.
                        Our platform market will store all the coins in the lending program and they will not be traded.
                        Diminishing the coin circulating in the market is the purpose of this system. The Billionaire
                        Coin price
                        in the market will be raised by the scarcity.
                        The price of the BILLIONAIRE COIN will be escalated significantly by the scarcity. The more the
                        price
                        will increase the fewer coins will be circulating.
                        The number of the coins in public will not be exceeded by the number of coins in lending program
                        and the
                        commission for each coin will come from a trusted source. There will be no money game in the
                        BILLIONAIRE
                        COIN lending program by applying to this system.
                        <br/>
                        <br/>
                        RULES FOR SAVING:
                    <ol>
                        <li>There is no coin minimum or maximum. All the coins in your wallet are safe.</li>
                        <li>The contract is for 60, or 90 days depending of the amount which is saved. BILLIONAIRE COIN
                            cannot
                            be withdrawn during the saving contract.
                        </li>
                        <li>Based on the lending or saving formula, daily commission will be allotted.</li>
                        <li>Commission will be distributed in BIL/BTC.</li>
                        <li>There will be daily commission available to be withdrawn every day.</li>
                        <li>When the lending contract is completed the same amount of BILLIONAIRE COIN that has been
                            lent will
                            be given back.
                        </li>
                    </ol>

                    </p>

                    <div class="row">
                        <div class="col-md-3 col-sm-6">
                            <div class="bil-price-all">
                                <div class="bil-price-head">
                                    <h4>Lending Amount</h4>

                                </div>
                                <div class="bil-price">
                                    <ul>
                                        <li class="styled">$100 - $1000</li>
                                        <li class="styled">$1010 - $5000</li>
                                        <li class="styled">$5010 - $10,000</li>
                                        <li class="styled">$10,0010 - $100,000</li>
                                        <li class="styled">$100,010 - above</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="bil-price-all">
                                <div class="bil-price-head">
                                    <h4>GUARANTEE</h4>

                                </div>
                                <div class="bil-price">
                                    <ul>
                                        <li class="styled">0.10%</li>
                                        <li class="styled">0.15%</li>
                                        <li class="styled">0.25%</li>
                                        <li class="styled">0.30%</li>
                                        <li class="styled">0.35%</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="bil-price-all">
                                <div class="bil-price-head">
                                    <h4>UP TO</h4>

                                </div>
                                <div class="bil-price">
                                    <ul>
                                        <li class="styled">48%</li>
                                        <li class="styled">48%</li>
                                        <li class="styled">48%</li>
                                        <li class="styled">48%</li>
                                        <li class="styled">48%</li>
                                    </ul>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-3 col-sm-6">
                            <div class="bil-price-all">
                                <div class="bil-price-head">
                                    <h4>DURATION</h4>
                                </div>
                                <div class="bil-price">
                                    <ul>
                                        <li class="styled">179 days</li>
                                        <li class="styled">149 days</li>
                                        <li class="styled">99 days</li>
                                        <li class="styled">60 days</li>
                                        <li class="styled">45 days</li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- END Lending or saving -->


            <!-- WhitePaper -->
            <div class="contain-wrapp padding-bottom-30 background-dark" id="whitepaper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="section-heading">
                                <h3>WHITEPAPER</h3>
                                <p>COMING SOON</p>
                                <i class="fa fa-user"></i>
                            </div>
                        </div>
                        <div class="countdown-time-whitepaper animated bounceIn" data-date="2018-01-15 00:00:00"
                             data-timer="900"></div>
                    </div>
                </div>
            </div>
            <!-- END WhitePaper -->
            <style>
                ol li {
                    color: #222222 !important;
                    font-family: "Open Sans", sans-serif;
                }
                .bil-price-all {
                    border: 1px solid;
                    background: #000;
                }
                .bil-price-head h4 {
                    margin-top: 10px;
                    max-height: 40px;
                    text-align: center;
                    text-transform: uppercase;
                    font-size: 18px !important;
                    border: none !important;
                }

                .bil-price ul li {
                    list-style: none;
                    color: #ffffff;
                    text-align: center;
                    text-transform: uppercase;
                    box-shadow: 0 5px 29px rgba(5, 2, 2, 0.71);
                    border-bottom: 1px solid #fff;
                    margin-right: 10px;
                    padding: 10px;

                }


                .styled {
                    color: #fff !important;
                }


            </style>

        @endsection



        @section('scripts')
            <!-- App scripts -->
                <script src="/scripts/luna.js"></script>


@endsection

