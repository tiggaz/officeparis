@extends('layouts.app')
@section('content')
    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <i class="pe page-header-icon pe-7s-config header-icon-45"></i>
                    </div>
                    <div class="pull-left">
                        <h3 class="m-b-xs">ICO</h3>
                    </div>
                </div>
            </div>

            <div class="row m-t-sm">
                <div class="col-md-12">
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                               Setup Next ICO
                            </h3>

                            @if(Session::has('flash_message'))
                                <div class="alert alert-success">
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif


                            <form method="POST" action="/adminico/store" accept-charset="UTF-8"><input
                                        name="_method" type="hidden" value="PUT">
                                {{ csrf_field() }}
                                <input name="action" type="hidden" value="store">
                                <div class="row">

                                    <div class="col-md-6">
                                        <label for="datetimepicker1" class="text-white">Date From: </label>
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control" name="from"/>
                                            <span class="input-group-addon">
                                                 <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="datetimepicker1" class="text-white">Date To: </label>
                                        <div class='input-group date' id='datetimepicker2'>
                                            <input type='text' class="form-control" name="to"/>
                                            <span class="input-group-addon">
                                                 <span class="glyphicon glyphicon-calendar"></span>
                                         </span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email" class="text-white">Total Bil Coins: </label>
                                            <input class="form-control" name="total" type="text" id="total">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email" class="text-white">Price: </label>
                                            <input class="form-control" name="price" type="text" id="price" placeholder="0.9">
                                        </div>
                                    </div>
                                </div>




                                <button type="submit" class="btn btn-accent">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                Send Transactions
                            </h3>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Available</th>
                                        <th>Total</th>
                                        <th>Sold</th>
                                        <th>Date</th>
                                        <th>Last sold at</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Price</th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                     @if(!$ico->first())
                                         <tr>
                                        <td colspan="8" class="bg-primary-blue">There is no ICO found
                                        </td>
                                         </tr>
                                    @else
                                        @foreach($ico as $index => $ico)
                                           <tr>
                                               <td>{{ $ico->available  }}</td>
                                               <td>{{ $ico->total  }}</td>
                                               <td>{{ $ico->sold  }}</td>
                                               <td>{{ $ico->date  }}</td>
                                               <td>{{ $ico->last_sold_at  }}</td>
                                               <td>{{ $ico->from  }}</td>
                                               <td>{{ $ico->to  }}</td>
                                               <td>{{ $ico->price  }}</td>
                                           </tr>
                                        @endforeach
                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- End main content-->



    <script>
        window.paceOptions = {
            ajax: false, // disabled
            document: false, // disabled
            eventLag: false, // disabled
        };
    </script>

    <!-- Vendor scripts -->
    <script src="/vendor/pacejs/pace.min.js"></script>
    <script src="/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/toastr/toastr.min.js"></script>
    <script src="/vendor/sparkline/index.js"></script>
    <script src="/vendor/flot/jquery.flot.min.js"></script>
    <script src="/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="/vendor/flot/jquery.flot.spline.js"></script>
    <script src="/vendor/d3/d3.min.js"></script>
    <script src="/vendor/topojson/topojson.min.js"></script>
    <script src="/vendor/datamaps/datamaps.world.min.js"></script>
    <script src="/vendor/moment/moment.js"></script>
    <script src="/vendor/datatables/datatables.min.js"></script>

    <!-- App scripts -->
    <script src="/scripts/luna.js"></script>
    <!-- Scripts -->
    <script>
        $(document).ready(function () {
            getInfo();
            function getInfo() {
                $.get('/ico/info', function (res) {
                    if (res.success) {
                        price = res.price;
                        bil_to_btc = res.bil_to_btc;
                        bil_to_usd = res.bil_to_usd;
                        btc_to_usd = res.btc_to_usd;


                        $('#bil_to_btc_nav').html(res.bil_to_btc);
                        $('#bil_to_usd_nav').html(res.bil_to_usd);
                        $('#btc_to_usd_nav').html(res.btc_to_usd);
                    }

                });
            }
        });
    </script>
    <style>
        .page-header-icon {
            font-size: 45px;
            padding-right: 10px;
            color: #FFFFFF;
        }
    </style>


    <script type="text/javascript">
        $(function () {
            $('#datetimepicker1').datetimepicker();
        });
    </script>

    <script type="text/javascript">
        $(function () {
            $('#datetimepicker2').datetimepicker();
        });
    </script>

    <script src="{{ mix('/js/app.js') }}"></script>
@endsection