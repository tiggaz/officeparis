@extends('layouts.app')

@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('head')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">


                <div class="btcwdgt-chart"></div>

                @include('panels.welcome-panel', ['address' =>  Auth::user()->AddressAssigned->address])

            </div>
        </div>
    </div>

@endsection