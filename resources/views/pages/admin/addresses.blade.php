@extends('layouts.app')

@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('head')
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">


                <div class="btcwdgt-chart"></div>
                <ul>
                    @foreach($addresses as $address)
                      <li> {{ $address->address }} </li>
                      <li> {{ $address->balance }} </li>
                      <li> {{ $address->label }} </li>
                      <li> {{ $address->total_requests }} </li>
                    @endforeach
                </ul>


            </div>
        </div>
    </div>

@endsection