@extends('layouts.app')

@section('template_title')
    {{ Auth::user()->name }}'s' Homepage
@endsection

@section('template_fastload_css')
@endsection

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-server"></i>
                        </div>
                        <div class="header-title">
                            <h3>Initial Coin Offering (ICO)</h3>

                            <div class="input-group">
                                <input id="ref_url" class="form-control" name="ref_url" type="text">
                                <span class="input-group-btn">
                                        <button data-copy-text="ref_url" class="btn btn-primary"
                                                type="button">Copy</button>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h2 class="m-b-none">
                                <p class="text-center base-font-color">DAY : HR : MIN : SEC</p>
                            </h2>
                            <h2 class="m-b-none">
                                <p class="text-center base-font-color">
                                    <span id="ico-countdown-dd">00</span> :
                                    <span id="ico-countdown-hh">00</span> :
                                    <span id="ico-countdown-mm">00</span> :
                                    <span id="ico-countdown-ss">00</span>
                                </p>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h2 class="m-b-none">BIL</h2>
                            <h3>
                                <p class="text-center">
                                    <strong class="app-bil c-accent">
                                        ...
                                    </strong><br>
                                    <small>ICO <span class="app-bil-ico">0.00</span> / BONUS <span
                                                class="app-bil-bonus">0.00</span></small>
                                </p>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h2 class="m-b-none">BTC</h2>
                            <h3>
                                <p class="text-center"><strong class="app-btc c-accent">...</strong><br>
                                    <small>&nbsp;</small>
                                </p>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h2 class="m-b-none"><p class="text-center base-font-color">Total Available Coin</p></h2>
                            <h3>
                                <p class="text-center">
                                    <strong id="app-total-available-coin">...</strong> BIL<br>
                                    <small>Total Sold <span class="app-total-sold">...</span> BIL</small>
                                </p>
                            </h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-filled">
                        <div id="time-box" class="panel-body">
                            <h4>
                                <i class="pe pe-7s-bell header-icon-30 m-b-xs"></i>
                                <span class="base-font-color">
                                <div id="closing-in-box" class="" style="color: white">
                                    Closing in
                                    <span id="ico-close-dd">00</span> days
                                    <span id="ico-close-hh">00</span> hrs
                                    <span id="ico-close-mm">00</span> mins
                                    <span id="ico-close-ss">00</span> sec.
                                </div>
                                <div id="opening-in-box" class="">
                                    Opening in
                                    <span id="ico-open-dd">00</span> days
                                    <span id="ico-open-hh">00</span> hrs
                                    <span id="ico-open-mm">00</span> mins
                                    <span id="ico-open-ss">00</span> sec.
                                </div>
                            </span>
                            </h4>


                        </div>
                    </div>
                </div>

                <div id="ico-sold-box" class="col-md-8 ">
                    <div class="panel panel-filled">
                        <div class="row">
                            <div class="col-md-4">

                                <div id="stats-box" class="panel-body h-200 list">
                                    <div class="stats-title">
                                        <h4>ICO Today : <span id="app-ico-today">...</span> BIL</h4>
                                        <h4>Sold : <span id="app-ico-sold">...</span> BIL</h4>
                                        <h4>Available : <span id="app-ico-available">...</span> BIL</h4>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="row m-b-md">
                <div class="col-md-12">
                    <div class="p-sm r-3 base-bg-image">
                        <div class="panel m-n">
                            <div class="panel-body">
                                <h3>Fill Amount</h3>

                                <form id="ico-form" action="">
                                    <div id="ico-form-success" class="alert alert-success hidden"></div>
                                    <div id="ico-form-error" class="alert alert-danger hidden"></div>
                                    <div id="ico-form-loading" class="alert alert-info hidden">Creating your
                                        order...
                                    </div>

                                    <div class="row m-b-sm">
                                        <div class="col-md-4">
                                            <h4>BIL</h4>
                                            <input id="bil_amount" class="form-control m-b-sm"
                                                   placeholder="Amount in BIL" name="bil_amount" type="text">
                                            <span class="text-white">Min = 50, Max = 1000 BIL (<a href="#"
                                                                                                  class="base-font-color"
                                                                                                  data-fill="#bil_amount"
                                                                                                  tabindex="-1">Buy All: <span
                                                            class="app-buy-all">...</span> BIL</a>)</span>
                                            <br>
                                            <span class="text-white">BIL amount must be multiples of 50 (i.e. 50, 100, 150, ...)</span>
                                        </div>
                                        <div class="col-md-4">
                                            <h4>BTC</h4>
                                            <input id="btc_amount" class="form-control" readonly tabindex="-1"
                                                   name="btc_amount" type="text">
                                        </div>
                                        <div class="col-md-4">
                                            <h4>USD</h4>
                                            <input id="usd_amount" class="form-control" readonly tabindex="-1"
                                                   name="usd_amount" type="text">
                                        </div>
                                    </div>

                                    <br>

                                    <button type="submit" class="btn btn-lg btn-accent">Buy BIL</button>
                                    <button type="reset" class="btn btn-lg btn-accent-default">Clear</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                My Orders
                            </h3>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="25%">Ordered At</th>
                                        <th width="25%">Amount (BIL)</th>
                                        <th width="25%">Processed At</th>
                                        <th width="25%">Status</th>
                                    </tr>
                                    </thead>

                                    <tbody id="ico-orders"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                Last 50 matched orders
                            </h3>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th width="33%">Processed At</th>
                                        <th width="33%">Address</th>
                                        <th width="34%">Amount (BIL)</th>
                                    </tr>
                                    </thead>

                                    <tbody id="completed-orders">
                                    <tr>
                                        <td colspan="3">
                                            <center>Available after ICO ends</center>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body">
                            <h4 class="m-t-n-sm m-b-xs">ICO activity</h4>
                            <samll>ICO sales by geography</samll>
                            <div id="serverMap"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End main content-->

    </div>
    <!-- End wrapper-->

    <!-- Scripts -->
    <script src="{{ mix('/js/app.js') }}"></script>

    <script>
        window.paceOptions = {
            ajax: false, // disabled
            document: false, // disabled
            eventLag: false, // disabled
        };
    </script>

    <!-- Vendor scripts -->
    <script src="/vendor/pacejs/pace.min.js"></script>
    <script src="/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/toastr/toastr.min.js"></script>
    <script src="/vendor/sparkline/index.js"></script>
    <script src="/vendor/flot/jquery.flot.min.js"></script>
    <script src="/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="/vendor/flot/jquery.flot.spline.js"></script>
    <script src="/vendor/d3/d3.min.js"></script>
    <script src="/vendor/topojson/topojson.min.js"></script>
    <script src="/vendor/datamaps/datamaps.world.min.js"></script>
    <script src="/vendor/moment/moment.js"></script>
    <script src="/vendor/datatables/datatables.min.js"></script>

    <!-- App scripts -->
    <script src="/scripts/luna.js"></script>


    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <script>
        var price = 0;
        var bil_to_btc = 0;
        var bil_to_usd = 0;
        var btc_to_usd = 0;

        var ico_date = null;
        var next_ico_date = null;

        var ico_date_interval = null;
        var next_ico_date_interval = null;

        $(document).ready(function () {
            $('[data-fill]').click(function (e) {
                e.preventDefault();
                $($(this).data('fill')).val($('.app-buy-all').first().text()).trigger('keyup')
            })

            function getUserInfo() {
                $.get('/user/info', function (res) {
                    if (res.success) {
                        $('#ref_url').val(res.ref_url)

                        $('.app-bil').text(res.bil)
                        $('.app-bil-ico').text(res.bil_ico)
                        $('.app-bil-bonus').text(res.bil_bonus)
                        $('.app-btc').text(res.btc)


                        var bil_amount = (res.btc / bil_to_btc).toFixed(0);

                        bil_amount -= (bil_amount % 50)
                        if (bil_amount > 1000) {
                            bil_amount = 1000
                        }


                        $('.app-buy-all').text(bil_amount)

                        var html = ''
                        var need_refresh = false;
                        $.each(res.ico_orders[0], function (key, item) {
                            html += '<tr>'
                                + '<td>' + item.created_at + '</td>'
                                + '<td>' + item.bil_amount + '</td>'
                                + '<td>' + (item.processed_at !== null ? item.processed_at : '') + '</td>'
                                + '<td>'
                                + (item.status === 0 ? '<span class="text-warning">In Queue</span>' : '')
                                + (item.status === 1 ? '<span class="text-success">Success</span>' : '')
                                + (item.status === -1 ? '<span class="text-danger">Cancelled - 200k sold out before your queue</span>' : '')
                                + '</td>'
                                + '</tr>';

                            if (item.status === 0) {
                                need_refresh = true;
                            }

                            if (need_refresh) {
                                setTimeout(function () {
                                    getIcoInfo();
                                    getUserInfo();
                                }, 33333)
                            }
                        })
                        $('#ico-orders').html(html)
                    }
                });
            }

            function getIcoInfo(first_time) {
                $.get('/ico/info', function (res) {
                    if (res.success) {
                        price = res.price;
                        bil_to_btc = res.bil_to_btc;
                        bil_to_usd = res.bil_to_usd;
                        btc_to_usd = res.btc_to_usd;

                        if (first_time) {
                            getUserInfo()
                        }

                        $('#bil_to_btc_nav').html(res.bil_to_btc);
                        $('#bil_to_usd_nav').html(res.bil_to_usd);
                        $('#btc_to_usd_nav').html(res.btc_to_usd);

                        $('#app-total-available-coin').text(res.total_available_coin)
                        $('.app-total-sold').text(res.total_sold)
                        $('#app-ico-today-sold').text(res.ico_today_sold)
                        $('#app-ico-today').text(res.ico_today)
                        $('#app-ico-sold').text(res.ico_sold)
                        $('#app-ico-available').text(res.ico_available)

                        if (res.next_ico_date) {
                            $('#opening-in-box').removeClass('hidden')
                            $('#closing-in-box').addClass('hidden')
                            $('#ico-sold-box').addClass('invisible')
                            ico_date = null
                            next_ico_date = res.next_ico_date

                            clearInterval(ico_date_interval)
                            var next_ico_date_interval = setInterval(function () {
                                if (next_ico_date === null) {
                                    return;
                                }

                                var now = new Date().getTime();

                                var targetId = 'ico-open';
                                var distance = next_ico_date.from_timestamp - now;


                                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                if (distance >= 0) {
                                    $('#' + targetId + '-dd').text(pad(days));
                                    $('#' + targetId + '-hh').text(pad(hours));
                                    $('#' + targetId + '-mm').text(pad(minutes));
                                    $('#' + targetId + '-ss').text(pad(seconds));
                                }
                            }, 1000);
                        } else if (res.ico_date) {
                            $('#opening-in-box').addClass('hidden')
                            $('#closing-in-box').removeClass('hidden')
                            $('#ico-sold-box').removeClass('invisible')
                            ico_date = res.ico_date
                            next_ico_date = null

                            clearInterval(next_ico_date_interval)
                            var ico_date_interval = setInterval(function () {
                                if (ico_date === null) {
                                    return;
                                }

                                var now = new Date().getTime();

                                var targetId = 'ico-close';
                                var distance = ico_date.to_timestamp - now;


                                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                if (distance >= 0) {
                                    $('#' + targetId + '-dd').text(pad(days));
                                    $('#' + targetId + '-hh').text(pad(hours));
                                    $('#' + targetId + '-mm').text(pad(minutes));
                                    $('#' + targetId + '-ss').text(pad(seconds));
                                }
                            }, 1000);
                        }

                        var html = ''
                        $.each(res.completed_orders[0], function (key, item) {
                            html += '<tr>'
                                + '<td>' + item.processed_at + '</td>'
                                + '<td>' + item.address + '</td>'
                                + '<td>' + item.bil_amount + '</td>'
                                + '</tr>';
                        })
                        if (html === '') {
                            html = '<tr><td colspan="3"><center>Available after ICO ends</center></td></tr>';
                        }
                        $('#completed-orders').html(html)
                    }
                });
            }

            getIcoInfo(true);

            countdown('ico-countdown', 1517353199000)

            $('#ico-form').submit(function (e) {
                e.preventDefault()

                if ($('#bil_amount').val() === '') {
                    alert('Please enter BIL amount.');
                    return;
                }


                $('#ico-form [type="submit"]').prop('disabled', true)
                $('#ico-form-success').addClass('hidden');
                $('#ico-form-error').addClass('hidden');
                $('#ico-form-loading').removeClass('hidden');


                $.ajax({
                    type: 'POST',
                    url: 'ico',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: $("#ico-form").serialize(),
                }).done(function (res) {
                    setTimeout(function () {
                        $('#ico-form [type="submit"]').prop('disabled', false)
                    }, 5555)
                    $('#ico-form-loading').addClass('hidden');

                    if (res.success) {

                        $('#ico-form-success').removeClass('hidden').html(res.success);
                        getUserInfo();
                        setTimeout(function () {
                            getIcoInfo();
                            getUserInfo();
                        }, 6666)
                    }

                    if (res.error) {
                        $('#ico-form-error').removeClass('hidden').html(res.error);


                    }

                }).fail(function () {
                    $('#ico-form-loading').addClass('hidden');
                    $('#ico-form-error').removeClass('hidden').html('Sorry! ICO is sold out.');
                })


            })
        })

        $('#time-box').height($('#stats-box').height())
        $(window).resize(function () {
            $('#time-box').height($('#stats-box').height())
        });

        jQuery(document).ready(function ($) {
            var isoCodeConverterData = {
                "BD": "BGD",
                "BE": "BEL",
                "BF": "BFA",
                "BG": "BGR",
                "BA": "BIH",
                "BB": "BRB",
                "WF": "WLF",
                "BL": "BLM",
                "BM": "BMU",
                "BN": "BRN",
                "BO": "BOL",
                "BH": "BHR",
                "BI": "BDI",
                "BJ": "BEN",
                "BT": "BTN",
                "JM": "JAM",
                "BV": "BVT",
                "BW": "BWA",
                "WS": "WSM",
                "BQ": "BES",
                "BR": "BRA",
                "BS": "BHS",
                "JE": "JEY",
                "BY": "BLR",
                "BZ": "BLZ",
                "RU": "RUS",
                "RW": "RWA",
                "RS": "SRB",
                "TL": "TLS",
                "RE": "REU",
                "TM": "TKM",
                "TJ": "TJK",
                "RO": "ROU",
                "TK": "TKL",
                "GW": "GNB",
                "GU": "GUM",
                "GT": "GTM",
                "GS": "SGS",
                "GR": "GRC",
                "GQ": "GNQ",
                "GP": "GLP",
                "JP": "JPN",
                "GY": "GUY",
                "GG": "GGY",
                "GF": "GUF",
                "GE": "GEO",
                "GD": "GRD",
                "GB": "GBR",
                "GA": "GAB",
                "SV": "SLV",
                "GN": "GIN",
                "GM": "GMB",
                "GL": "GRL",
                "GI": "GIB",
                "GH": "GHA",
                "OM": "OMN",
                "TN": "TUN",
                "JO": "JOR",
                "HR": "HRV",
                "HT": "HTI",
                "HU": "HUN",
                "HK": "HKG",
                "HN": "HND",
                "HM": "HMD",
                "VE": "VEN",
                "PR": "PRI",
                "PS": "PSE",
                "PW": "PLW",
                "PT": "PRT",
                "SJ": "SJM",
                "PY": "PRY",
                "IQ": "IRQ",
                "PA": "PAN",
                "PF": "PYF",
                "PG": "PNG",
                "PE": "PER",
                "PK": "PAK",
                "PH": "PHL",
                "PN": "PCN",
                "PL": "POL",
                "PM": "SPM",
                "ZM": "ZMB",
                "EH": "ESH",
                "EE": "EST",
                "EG": "EGY",
                "ZA": "ZAF",
                "EC": "ECU",
                "IT": "ITA",
                "VN": "VNM",
                "SB": "SLB",
                "ET": "ETH",
                "SO": "SOM",
                "ZW": "ZWE",
                "SA": "SAU",
                "ES": "ESP",
                "ER": "ERI",
                "ME": "MNE",
                "MD": "MDA",
                "MG": "MDG",
                "MF": "MAF",
                "MA": "MAR",
                "MC": "MCO",
                "UZ": "UZB",
                "MM": "MMR",
                "ML": "MLI",
                "MO": "MAC",
                "MN": "MNG",
                "MH": "MHL",
                "MK": "MKD",
                "MU": "MUS",
                "MT": "MLT",
                "MW": "MWI",
                "MV": "MDV",
                "MQ": "MTQ",
                "MP": "MNP",
                "MS": "MSR",
                "MR": "MRT",
                "IM": "IMN",
                "UG": "UGA",
                "TZ": "TZA",
                "MY": "MYS",
                "MX": "MEX",
                "IL": "ISR",
                "FR": "FRA",
                "IO": "IOT",
                "SH": "SHN",
                "FI": "FIN",
                "FJ": "FJI",
                "FK": "FLK",
                "FM": "FSM",
                "FO": "FRO",
                "NI": "NIC",
                "NL": "NLD",
                "NO": "NOR",
                "NA": "NAM",
                "VU": "VUT",
                "NC": "NCL",
                "NE": "NER",
                "NF": "NFK",
                "NG": "NGA",
                "NZ": "NZL",
                "NP": "NPL",
                "NR": "NRU",
                "NU": "NIU",
                "CK": "COK",
                "XK": "XKX",
                "CI": "CIV",
                "CH": "CHE",
                "CO": "COL",
                "CN": "CHN",
                "CM": "CMR",
                "CL": "CHL",
                "CC": "CCK",
                "CA": "CAN",
                "CG": "COG",
                "CF": "CAF",
                "CD": "COD",
                "CZ": "CZE",
                "CY": "CYP",
                "CX": "CXR",
                "CR": "CRI",
                "CW": "CUW",
                "CV": "CPV",
                "CU": "CUB",
                "SZ": "SWZ",
                "SY": "SYR",
                "SX": "SXM",
                "KG": "KGZ",
                "KE": "KEN",
                "SS": "SSD",
                "SR": "SUR",
                "KI": "KIR",
                "KH": "KHM",
                "KN": "KNA",
                "KM": "COM",
                "ST": "STP",
                "SK": "SVK",
                "KR": "KOR",
                "SI": "SVN",
                "KP": "PRK",
                "KW": "KWT",
                "SN": "SEN",
                "SM": "SMR",
                "SL": "SLE",
                "SC": "SYC",
                "KZ": "KAZ",
                "KY": "CYM",
                "SG": "SGP",
                "SE": "SWE",
                "SD": "SDN",
                "DO": "DOM",
                "DM": "DMA",
                "DJ": "DJI",
                "DK": "DNK",
                "VG": "VGB",
                "DE": "DEU",
                "YE": "YEM",
                "DZ": "DZA",
                "US": "USA",
                "UY": "URY",
                "YT": "MYT",
                "UM": "UMI",
                "LB": "LBN",
                "LC": "LCA",
                "LA": "LAO",
                "TV": "TUV",
                "TW": "TWN",
                "TT": "TTO",
                "TR": "TUR",
                "LK": "LKA",
                "LI": "LIE",
                "LV": "LVA",
                "TO": "TON",
                "LT": "LTU",
                "LU": "LUX",
                "LR": "LBR",
                "LS": "LSO",
                "TH": "THA",
                "TF": "ATF",
                "TG": "TGO",
                "TD": "TCD",
                "TC": "TCA",
                "LY": "LBY",
                "VA": "VAT",
                "VC": "VCT",
                "AE": "ARE",
                "AD": "AND",
                "AG": "ATG",
                "AF": "AFG",
                "AI": "AIA",
                "VI": "VIR",
                "IS": "ISL",
                "IR": "IRN",
                "AM": "ARM",
                "AL": "ALB",
                "AO": "AGO",
                "AQ": "ATA",
                "AS": "ASM",
                "AR": "ARG",
                "AU": "AUS",
                "AT": "AUT",
                "AW": "ABW",
                "IN": "IND",
                "AX": "ALA",
                "AZ": "AZE",
                "IE": "IRL",
                "ID": "IDN",
                "UA": "UKR",
                "QA": "QAT",
                "MZ": "MOZ"
            };

            var originalDatamapsData = {

            @if($countries)
            @foreach($countries as $country)
            {{ $country }}:
            {
                fillKey: "active"
            }
            ,
            @endforeach
            @endif
        }
            ;

            function convertDataToTwoLetterIsoCode(datamapsData) {
                var converted = {};
                for (var data in datamapsData) {
                    if (datamapsData.hasOwnProperty(data)) {
                        converted[isoCodeConverterData[data]] = datamapsData[data];
                    }
                }
                return converted;
            }

            var convertedData = convertDataToTwoLetterIsoCode(originalDatamapsData);

            var wordmap = new Datamap({
                element: document.getElementById('serverMap'),
                fills: {
                    defaultFill: "#3B3D46",
                    active: "#F5A212"
                },
                responsive: true,
                geographyConfig: {
                    highlightOnHover: false,
                    borderWidth: 0

                },
                data: convertedData
            });
        });
    </script>


    <script>
        function pad(n) {
            var width = 2;
            var z = '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }
        function countdown(targetId, targetDate) {

            var interval = setInterval(function () {
                var now = new Date().getTime();
                var distance = targetDate - now;
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                $('#' + targetId + '-dd').text(pad(days));
                $('#' + targetId + '-hh').text(pad(hours));
                $('#' + targetId + '-mm').text(pad(minutes));
                $('#' + targetId + '-ss').text(pad(seconds));
                if (distance < 0) {
                    clearInterval(interval);
                }
            }, 1000);
        }
        $('[data-copy-text]').click(function () {
            $(this).text('Copied!');
            var id = $(this).data('copy-text');
            copyToClipboard(document.getElementById(id));
            setTimeout(function () {
                $('[data-copy-text=' + id + ']').text('Copy');
            }, 1000);
        })
        function copyToClipboard(elem) {
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch (e) {
                succeed = false;
            }
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }
            if (isInput) {
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                target.textContent = "";
            }
            return succeed;
        }
        $(document).ready(function () {
            $('#bil_amount').keyup(function () {
                var bil_amount = $(this).val();
                var usd_amount = (bil_amount * price).toFixed(2);
                var btc_amount = (usd_amount / btc_to_usd).toFixed(8);
                $('#btc_amount').val(btc_amount);
                $('#usd_amount').val(usd_amount);
            })
            $('[name="enable2fa"]').change(function () {
                if ($(this).val() == '1') {
                    $('#2fa-options').collapse('show')
                } else {
                    $('#2fa-options').collapse('hide')
                }
            })
        })
    </script>

    <style>
        .base-bg-image {
            background-image: url('/images/background-2.jpg');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: cover;
            box-shadow: #0a0a0a 0px 0px 30px;
        }

        .r-3 {
            border-radius: 3px;
        }

        .p-sm {
            padding: 15px !important;
        }
    </style>
@endsection