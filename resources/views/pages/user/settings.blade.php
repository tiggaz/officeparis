@extends('layouts.app')
@section('content')
    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="pull-left">
                        <i class="pe page-header-icon pe-7s-config header-icon-45"></i>
                    </div>
                    <div class="pull-left">
                        <h3 class="m-b-xs">Setting</h3>
                    </div>
                </div>
            </div>

            <div class="row m-t-sm">
                <div class="col-md-12">
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                My Profile
                            </h3>

                            @if(Session::has('flash_message'))
                                <div class="alert alert-success">
                                    {{ Session::get('flash_message') }}
                                </div>
                            @endif


                            <form method="POST" action="/user/update" accept-charset="UTF-8"><input
                                        name="_method" type="hidden" value="PUT">
                                {{ csrf_field() }}
                                <input name="action" type="hidden" value="update">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="username" class="text-white">Username</label>
                                            <input class="form-control" readonly name="name" type="text"
                                                   value="{{ Auth()->user()->name }}" id="username">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email" class="text-white">Email</label>
                                            <input class="form-control" readonly name="email" type="text"
                                                   value="{{ Auth()->user()->email }}" id="email">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="text-white">Name</label>
                                            <input class="form-control" name="first_name" type="text"
                                                   value="{{ Auth()->user()->first_name }}" id="first_name">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name" class="text-white">Name</label>
                                            <input class="form-control" name="last_name" type="text"
                                                   value="{{ Auth()->user()->last_name }}" id="last_name">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone" class="text-white">Phone</label>
                                            <input class="form-control" name="phone" type="text" value="201334224"
                                                   id="phone">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="country" class="text-white">Country</label>
                                            <select class="form-control" name="country">
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" @if(Auth()->user()->country == $country->id) selected @endif>{{ $country->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>

                                <button type="submit" class="btn btn-accent">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                My Password
                            </h3>

                            @if(Session::has('flash_message_pass'))
                                <div class="alert alert-success">
                                    {{ Session::get('flash_message_pass') }}
                                </div>
                            @endif

                            @if(Session::has('flash_message_wrong'))
                                <div class="alert alert-danger">
                                    {{ Session::get('flash_message_wrong') }}
                                </div>
                            @endif

                            <form method="POST" action="/user/update/password" accept-charset="UTF-8">
                                <input name="_method" type="hidden" value="PUT">
                                {{ csrf_field() }}
                                <input name="action" type="hidden" value="update-password">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
                                            <label for="current_password" class="text-white">Current Password</label>
                                            <input class="form-control" name="current_password" type="password" value=""
                                                   id="current_password">
                                        </div>
                                        @if ($errors->has('current_password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('current_password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="text-white">New Password</label>
                                            <input class="form-control" name="password" type="password" value=""
                                                   id="password">
                                        </div>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                            <label for="password_confirmation" class="text-white">Confirm
                                                Password</label>
                                            <input class="form-control" name="password_confirmation" type="password"
                                                   value="" id="password_confirmation">
                                        </div>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    <div class="col-md-6">

                                    </div>
                                </div>

                                <button type="submit" class="btn btn-accent">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End main content-->



    <script>
        window.paceOptions = {
            ajax: false, // disabled
            document: false, // disabled
            eventLag: false, // disabled
        };
    </script>

    <!-- Vendor scripts -->
    <script src="/vendor/pacejs/pace.min.js"></script>
    <script src="/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/toastr/toastr.min.js"></script>
    <script src="/vendor/sparkline/index.js"></script>
    <script src="/vendor/flot/jquery.flot.min.js"></script>
    <script src="/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="/vendor/flot/jquery.flot.spline.js"></script>
    <script src="/vendor/d3/d3.min.js"></script>
    <script src="/vendor/topojson/topojson.min.js"></script>
    <script src="/vendor/datamaps/datamaps.world.min.js"></script>
    <script src="/vendor/moment/moment.js"></script>
    <script src="/vendor/datatables/datatables.min.js"></script>

    <!-- App scripts -->
    <script src="/scripts/luna.js"></script>
    <!-- Scripts -->
    <script>
        $(document).ready(function () {
            getInfo();
            function getInfo() {
                $.get('/ico/info', function (res) {
                    if (res.success) {
                        price = res.price;
                        bil_to_btc = res.bil_to_btc;
                        bil_to_usd = res.bil_to_usd;
                        btc_to_usd = res.btc_to_usd;


                        $('#bil_to_btc_nav').html(res.bil_to_btc);
                        $('#bil_to_usd_nav').html(res.bil_to_usd);
                        $('#btc_to_usd_nav').html(res.btc_to_usd);
                    }

                });
            }
        });
    </script>
    <style>
        .page-header-icon {
            font-size: 45px;
            padding-right: 10px;
            color: #FFFFFF;
        }
    </style>

    <script src="{{ mix('/js/app.js') }}"></script>
@endsection