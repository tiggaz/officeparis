@extends('layouts.app')
@section('content')
    <!-- Main content-->
    <section class="content">
        <div class="base-bg-image p-xl content-title">
            <div class="row">
                <div class="col-md-12">
                    <img class="img-circle center-block" src="/images/logo.png" data-holder-rendered="true" style="max-width: 250px;">
                </div>
            </div>

            <div class="row m-t-md">
                <div class="col-md-12">
                    <h1>
                        <p class="text-center base-font-color">{{ number_format($bilAmount, 8, '.', '') }}
                            <small class="base-font-color">BIL</small>
                        </p>
                    </h1>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h3>
                        <p class="text-center">{!! $btcBallance->balance !!}
                            <small>BTC</small>
                        </p>
                    </h3>
                </div>
            </div>

            <div class="row m-t-xl">
                <div class="col-md-12">
                    <p class="text-center">
                        <button class="btn btn-accent btn-lg m-b-sm" type="submit" data-toggle="modal"
                                data-target="#deposit-bitcoin">Deposit Bitcoin (BTC)
                        </button>
                        <button class="btn btn-accent btn-lg m-l-sm m-b-sm" type="submit" data-toggle="modal"
                                data-target="#deposit-bilcoin">Deposit Bilcoin (BIL)
                        </button>
                    </p>
                </div>
            </div>
        </div>

        <div class="container-fluid">


            <div class="row m-t-xl">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                Deposit Transactions
                            </h3>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Address</th>
                                        <th>Amount</th>
                                        <th>Confirmation</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @if(!$spent)
                                        <td colspan="3" class="bg-primary-blue">No incoming Bitcoin transaction
                                            found
                                        </td>
                                    @else
                                        @foreach($spent as $index => $spent)
                                            @if(env('WALLET_ADDRESS') != $spent->outputs[0]->address)
                                            <tr>
                                                <td>{{ $spent->outputs[0]->address }}</td>
                                                <td>{{ $spent->outputs[0]->value }}</td>
                                                <td>@if($spent->block_height == null)
                                                        0/3 @else {{ $confirmationsSpent[$index] }}/3 @endif</td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                Send Transactions
                            </h3>

                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Address</th>
                                        <th>Amount</th>
                                        <th>Confirmation</th>
                                        <th>Requested At</th>
                                        <th>Address</th>
                                        <th>BTC</th>
                                        <th>Completed At</th>
                                        <th>TXID</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <!--
                                     @if(!$received)
                                        <td colspan="3" class="bg-primary-blue">No incoming Bitcoin transaction
                                            found
                                        </td>
                                    @else
                                        @foreach($received as $index => $received)
                                            @if(env('WALLET_ADDRESS') != $received->outputs[0]->address)
                                                <tr>
                                                    <td>{{ $received->outputs[0]->address }}</td>
                                                <td>{{ $received->outputs[0]->value }}</td>
                                                <td>@if($received->block_height == null)
                                                    0/3 @else {{ $confirmationsReceived[$index] }}/3 @endif</td>
                                            </tr>
                                            @endif
                                        @endforeach
                                    @endif
                                    -->
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                Send Bitcoin (BTC)
                            </h3>

                            <div class="alert alert-warning">
                                Please note that withdrawing BTC will be available after you have purchased at least 50
                                BIL from ICO. Otherwise, You have to wait for January, 30th 2018
                            </div>


                            <form method="POST" action="#"
                                  accept-charset="UTF-8">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="address" class="text-white">To Address</label>
                                    <input class="form-control" name="bitaddress" type="text" id="bitaddress">
                                </div>
                                <div class="form-group">
                                    <label for="amount" class="text-white">Amount in Bitcoin</label>
                                    <input class="form-control" name="price" type="text" id="price">
                                    <span class="text-white">Fee: 0.0005 BTC</span>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="text-white">Password</label>
                                    <input class="form-control" autocomplete="new-password" name="bilpassword"
                                           type="password" value="" id="bilpassword">
                                </div>
                                <button type="submit" class="btn btn-accent">Withdraw from BTC wallet</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <h3 class="m-t-md">
                                Send BILCoin (BIL)
                            </h3>


                            <form method="POST" action="#"
                                  accept-charset="UTF-8"><input name="_token" type="hidden"
                                                                value="SXJxfznbmkCETV0E7NxoEYUlr7vz1UGCXYmV9zmJ">
                                <div class="form-group">
                                    <label for="address" class="text-white">To Address</label>
                                    <input class="form-control" name="address" type="text" id="address">
                                </div>
                                <div class="form-group">
                                    <label for="amount" class="text-white">Amount in BILCoin</label>
                                    <input class="form-control" name="amount" type="text" id="amount">
                                    &nbsp;
                                </div>
                                <div class="form-group">
                                    <label for="password" class="text-white">Password</label>
                                    <input class="form-control" name="password" type="password" value="" id="password">
                                </div>
                                <button type="submit" class="btn btn-accent">Withdraw from BIL wallet</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="deposit-bitcoin" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title base-font-color">Deposit Bitcoin (BTC)</h4>
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-warning">
                                Please note that withdrawing BTC will be available after you have purchased at least 50
                                BIL from ICO. Otherwise, You have to wait for January, 30th 2018
                            </div>

                            <img class="bg-white center-block p-xxs"
                                 src="{{ $btcAddressQR }}"
                                 alt="barcode">

                            <h4 class="m-t-xl"><p class="text-center">YOUR BITCOIN ADDRESS</p></h4>

                            <div class="input-group p-b-10">
                                <input id="btc_address" class="form-control" name="btc_address" type="text"
                                       value="{{ $btcAddress }}">
                                <span class="input-group-btn">
                                <button data-copy-text="btc_address" class="btn btn-primary"
                                        type="button">Copy</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="deposit-bilcoin" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title base-font-color">Deposit Bilcoin (BIL)</h4>
                        </div>
                        <div class="modal-body">
                            <img class="bg-white center-block p-xxs"
                                 src="{{ $bilAddressQR }}"
                                 alt="barcode">

                            <h4 class="m-t-xl"><p class="text-center">YOUR BILCOIN ADDRESS</p></h4>

                            <div class="input-group p-b-10">
                                <input id="bil_address" class="form-control" name="bil_address" type="text"
                                       value="{{ $bilAddress }}">
                                <span class="input-group-btn">
                                <button data-copy-text="bil_address" class="btn btn-primary" type="button">Copy</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End main content-->

    </div>
    <!-- End wrapper-->


    <script>
        window.paceOptions = {
            ajax: false, // disabled
            document: false, // disabled
            eventLag: false, // disabled
        };
    </script>

    <!-- Vendor scripts -->
    <script src="/vendor/pacejs/pace.min.js"></script>
    <script src="/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/toastr/toastr.min.js"></script>
    <script src="/vendor/sparkline/index.js"></script>
    <script src="/vendor/flot/jquery.flot.min.js"></script>
    <script src="/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="/vendor/flot/jquery.flot.spline.js"></script>
    <script src="/vendor/d3/d3.min.js"></script>
    <script src="/vendor/topojson/topojson.min.js"></script>
    <script src="/vendor/datamaps/datamaps.world.min.js"></script>
    <script src="/vendor/moment/moment.js"></script>
    <script src="/vendor/datatables/datatables.min.js"></script>

    <!-- App scripts -->
    <script src="/scripts/luna.js"></script>
    <!-- Scripts -->


    <script>

        $('[data-copy-text]').click(function () {
            $(this).text('Copied!');
            var id = $(this).data('copy-text');
            copyToClipboard(document.getElementById(id));
            setTimeout(function () {
                $('[data-copy-text=' + id + ']').text('Copy');
            }, 1000);
        });

        function copyToClipboard(elem) {
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);
            var succeed;
            try {
                succeed = document.execCommand("copy");
            } catch (e) {
                succeed = false;
            }
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }
            if (isInput) {
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                target.textContent = "";
            }
            return succeed;
        }
    </script>
    <script src="{{ mix('/js/app.js') }}"></script>

    <script>
        $(document).ready(function () {
            getInfo();
            function getInfo() {
                $.get('/ico/info', function (res) {
                    if (res.success) {
                        price = res.price;
                        bil_to_btc = res.bil_to_btc;
                        bil_to_usd = res.bil_to_usd;
                        btc_to_usd = res.btc_to_usd;


                        $('#bil_to_btc_nav').html(res.bil_to_btc);
                        $('#bil_to_usd_nav').html(res.bil_to_usd);
                        $('#btc_to_usd_nav').html(res.btc_to_usd);
                    }

                });
            }
        });
    </script>
    <style>
        .base-bg-image {
            background-image: url('/images/background-2.jpg');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: cover;
            box-shadow: #0a0a0a 0px 0px 30px;
        }

        .r-3 {
            border-radius: 3px;
        }

        .p-sm {
            padding: 15px !important;
        }
    </style>
@endsection