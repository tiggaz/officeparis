@extends('layouts.app')
@section('content')
    <!-- Main content-->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <p clas="text-center">Under development</p>
                </div>
            </div>
        </div>
    </section>
    <!-- End main content-->



    <script>
        window.paceOptions = {
            ajax: false, // disabled
            document: false, // disabled
            eventLag: false, // disabled
        };
    </script>

    <!-- Vendor scripts -->
    <script src="/vendor/pacejs/pace.min.js"></script>
    <script src="/vendor/jquery/dist/jquery.min.js"></script>
    <script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="/vendor/toastr/toastr.min.js"></script>
    <script src="/vendor/sparkline/index.js"></script>
    <script src="/vendor/flot/jquery.flot.min.js"></script>
    <script src="/vendor/flot/jquery.flot.resize.min.js"></script>
    <script src="/vendor/flot/jquery.flot.spline.js"></script>
    <script src="/vendor/d3/d3.min.js"></script>
    <script src="/vendor/topojson/topojson.min.js"></script>
    <script src="/vendor/datamaps/datamaps.world.min.js"></script>
    <script src="/vendor/moment/moment.js"></script>
    <script src="/vendor/datatables/datatables.min.js"></script>

    <!-- App scripts -->
    <script src="/scripts/luna.js"></script>
    <!-- Scripts -->
    <script>
        $(document).ready(function () {
            getInfo();
            function getInfo() {
                $.get('/ico/info', function (res) {
                    if (res.success) {
                        price = res.price;
                        bil_to_btc = res.bil_to_btc;
                        bil_to_usd = res.bil_to_usd;
                        btc_to_usd = res.btc_to_usd;


                        $('#bil_to_btc_nav').html(res.bil_to_btc);
                        $('#bil_to_usd_nav').html(res.bil_to_usd);
                        $('#btc_to_usd_nav').html(res.btc_to_usd);
                    }

                });
            }
        });
    </script>
    <style>
        .page-header-icon {
            font-size: 45px;
            padding-right: 10px;
            color: #FFFFFF;
        }
    </style>

    <script src="{{ mix('/js/app.js') }}"></script>
@endsection