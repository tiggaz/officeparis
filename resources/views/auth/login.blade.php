@extends('layouts.login')

@section('content')

    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                       autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                @endif
            </div>
        </div>


                        <input type="hidden" id="checkbox" class="checkbox" name="remember" checked>

        @if(config('settings.reCaptchStatus'))
            <div class="form-group">
                <div class="col-sm-6 col-sm-offset-4">
                    <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}" data-theme="dark"></div>
                </div>
                @if ($errors->has('captcha'))
                    <div class="col-sm-6 col-sm-offset-4 control-label">
                <span class="help-block">
                                        <label class="warning" style="color: firebrick">{{ $errors->first('captcha') }}</label>
                                    </span>
                    </div>
                @endif
            </div>
        @endif



        <div class="form-group margin-bottom-3">
            <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Login
                </button>
                <a class="btn btn-default" href="/register">Register</a>
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
            </div>
        </div>


    </form>

@endsection

@section('footer_scripts')

    <script src='https://www.google.com/recaptcha/api.js'></script>

@endsection