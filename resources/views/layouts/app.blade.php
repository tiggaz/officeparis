<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">




        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Billionaire Coin</title>
        <meta name="description" content="">
        <meta name="author" content="Billionaire Coin">
        <link rel="icon" href="/images/favicon.png">

        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>


        {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

        <![endif]-->

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        {{-- Styles --}}
        <link href="{{ mix('/css/app.css') }}" rel="stylesheet">

        <!-- Vendor styles -->
        <link rel="stylesheet" href="/vendor/fontawesome/css/font-awesome.css"/>
        <link rel="stylesheet" href="/vendor/animate.css/animate.css"/>
        <link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.css"/>
        <link rel="stylesheet" href="/vendor/toastr/toastr.min.css"/>

        <!-- App styles -->
        <link rel="stylesheet" href="/styles/pe-icons/pe-icon-7-stroke.css"/>
        <link rel="stylesheet" href="/styles/pe-icons/helper.css"/>
        <link rel="stylesheet" href="/styles/stroke-icons/style.css"/>
        <link rel="stylesheet" href="/styles/style.css"/>
        <link rel="stylesheet" href="/css/style.css"/>


        <link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

        {{-- Scripts --}}
        <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>

        @yield('head')

    </head>
    <body>
        <div id="app">

            @include('partials.nav')


                @include('partials.form-status')


            @yield('content')

        </div>

        {{-- Scripts --}}

        <script src="{{ mix('/js/app.js') }}"></script>
        {!! HTML::script('//maps.googleapis.com/maps/api/js?key='.env("GOOGLEMAPS_API_KEY").'&libraries=places&dummy=.js', array('type' => 'text/javascript')) !!}



        @yield('footer_scripts')



        <!-- App scripts -->

        <script src="/js/jquery.countdown.min.js"></script>

        <script type="text/javascript" src="/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <div id="getting-started"></div>
        <script type="text/javascript">
            $("#clock")
                .countdown("2018/01/01", function(event) {
                    $(this).text(
                        event.strftime('%D : %H : %M : %S')
                    );
                });
        </script>

    </body>
</html>