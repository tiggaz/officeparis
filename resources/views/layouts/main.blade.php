<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="bitcoin, billionaire coin, coins, cryptocurrency, club">
    <meta name="description" content="Billionare Coin Is Essentially The Bitcoin Of The Middle East">
    <meta name="author" content="Billionaire Club">
    <link rel="icon" href="/images/favicon.png">

    <title>Billionaire Coin</title>

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="Billionaire Coin">
    <meta itemprop="description" content="Billionare Coin Is Essentially The Bitcoin Of The Middle East">
    <meta itemprop="image" content="https://billionairecoin.co/images/billionairecoin.png">

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@BillionaireCoin">
    <meta name="twitter:title" content="Billionare Coin">
    <meta name="twitter:description" content="The Bitcoin Of The Middle East">
    <meta name="twitter:creator" content="@BillionaireCoin">
    <meta name="twitter:image" content="https://billionairecoin.co/images/billionairecoin.png">

    <!-- Open Graph data -->
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="Billionaire Coin"/>
    <meta property="og:type" content="cryptocurrency"/>
    <meta property="og:url" content="https://billionairecoin.co"/>
    <meta property="og:image" content="https://billionairecoin.co/images/billionairecoin.png"/>
    <meta property="og:description" content="Billionare Coin Is Essentially The Bitcoin Of The Middle East"/>
    <meta property="og:site_name" content="Billionaire Coin"/>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="/assets/js/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/assets/css/revolution/settings.css">

    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/assets/css/revolution/layers.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/revolution/navigation.css">
    <link rel="stylesheet" type="text/css" href="/css/flipclock.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/jquery.countdown.css">

    <!-- Custom styles for this template -->
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/css/custom.css" rel="stylesheet">

    <!-- Theme skins -->
    <link id="skin" href="/assets/css/theme-colors/yellow.css" rel="stylesheet">
    <link href="/assets/js/style-switcher/css/style-switcher.css" rel="stylesheet">
    <link href="/assets/css/line-icons/line-icons.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/assets/js/html5shiv.min.js"></script>
    <script src="/assets/js/respond.min.js"></script>
    <![endif]-->

    <!--[if IE 9]>
    <link href="/assets/css/ie.css" rel="stylesheet">
    <![endif]-->
</head>

<body id="home">

<!-- Start top area -->
<div class="top-container">
    <div class="container">
        <div class="top-column-left">
            <ul class="contact-line">
                <li><i class="fa fa-envelope"></i> support@billionairecoin.co</li>
            </ul>
        </div>
        <div class="top-column-right">
            <div class="top-social-network">
                <a href="https://t.me/billionariecoin"><i class="fa fa-send-o"></i></a>
            </div>
            <ul class="register">

                @if (Route::has('login'))
                    <div class="top-right links">
                        @if (Auth::check())
                            <li><a href="{{ url('/home') }}">Home</a></li>
                        @else
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @endif
                    </div>
                @endif

            </ul>
        </div>
    </div>
</div>
<!-- End top area -->

<div class="clearfix"></div>

<!-- START -  Navbar -->
<nav class="navbar navbar-default navbar-dark megamenu">
    <div class="container">

        <!-- END - Navbar Right -->

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand logo" href="/">
                <img src="/images/logo.png" alt="Billionaire Coin" style="max-width: 60px;"/>
            </a>
            <p style="float: right; color: #fff !important; text-transform: uppercase; margin-top: 30px;">Billionaire
                Coin</p>
        </div>

        <div class="hidden-xs hidden-sm">
            <!-- START - Form Search -->
            <div class="search-wrapper animated">
                <input type="text" class="form-search" placeholder="Type something and hit enter...">
            </div>
            <!-- END - Form Search -->
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <ul class="nav navbar-nav navbar-right" data-in="fadeInLeft">
                <li class="active scroll"><a href="#home">HOME</a></li>
                <li class="scroll"><a href="#about">ABOUT</a></li>
                <li class="scroll"><a href="#ico">ICO</a></li>
                <li class="scroll"><a href="#roadmap">ROADMAP</a></li>
                <li class="scroll"><a href="#landing">LENDING</a></li>
                <li class="scroll"><a href="#whitepaper">WHITEPAPER</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
</nav>
<!-- END - Navbar -->

<div class="hidden-xs hidden-sm">
    <!-- START REVOLUTION SLIDER 5.0 -->
    <div id="slider_container" class="rev_slider_wrapper">
        <div id="rev-slider" class="rev_slider" data-version="5.0">
            <ul>
                <li data-transition="slideremovedown">
                    <!-- MAIN IMAGE -->
                    <img src="/images/nice2.jpg" alt="" width="1920" height="600">
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption captionHeadline1" id="slide-397-layer-1" data-x="center" data-hoffset="0"
                         data-y="top" data-voffset="0" data-width="['auto','auto','auto','auto']"
                         data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;"
                         data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="2000" data-splitin="none"
                         data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap;"><img
                                src="/images/logo.png" alt="Billionaire Coin"/>
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption captionHeadline2" id="slide-397-layer-2" data-x="center" data-hoffset="0"
                         data-y="top" data-voffset="330" data-width="['auto','auto','auto','auto']"
                         data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;"
                         data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3500" data-splitin="none"
                         data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><h6>
                            Billionaire Coin Is Essentially The Bitcoin Of The Middle East - Forbes</h6>
                    </div>

                    <!-- LAYER NR. 3 -->
                    <div class="buttons-slider">
                        <div class="text-center">
                            <a href="/login" class="btn-e btn-e-primary">Login <i class="fa fa-user"></i></a> <a
                                    href="/register" class="btn-e">Register <i class="fa fa-chain"></i></a>
                        </div>
                        <div class="countdown-time animated bounceIn" data-date="2018-01-28 00:00:00"
                             data-timer="900"></div>
                        <div class="text-center">
                            <a href="#ico" class="btn-b">ICO START <i class="fa fa-time"></i></a>
                        </div>
                    </div>


                </li>


                <li data-transition="slideremovedown">
                    <!-- MAIN IMAGE -->
                    <img src="/images/nice2.jpg" alt="" width="1920" height="600">
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption captionHeadline1" id="slide-397-layer-1" data-x="center" data-hoffset="0"
                         data-y="top" data-voffset="0" data-width="['auto','auto','auto','auto']"
                         data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;"
                         data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="2000" data-splitin="none"
                         data-splitout="none" data-responsive_offset="on" style="z-index: 8; white-space: nowrap;"><img
                                src="/images/logo.png" alt="Billionaire Coin"/>
                    </div>
                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption captionHeadline2" id="slide-397-layer-2" data-x="center" data-hoffset="0"
                         data-y="top" data-voffset="330" data-width="['auto','auto','auto','auto']"
                         data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;"
                         data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-transform_out="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                         data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="3500" data-splitin="none"
                         data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap;"><h6>
                            Join the Billionaire Coin today, and become a Billionaire tomorrow.</h6>
                    </div>
                    <!-- LAYER NR. 3 -->
                    <div class="buttons-slider">
                        <div class="text-center">
                            <a href="/login" class="btn-e btn-e-primary">Login <i class="fa fa-user"></i></a> <a
                                    href="/register" class="btn-e">Register <i class="fa fa-chain"></i></a>
                        </div>

                        <div class="countdown-time animated bounceIn" data-date="2018-01-28 00:00:00"
                             data-timer="900"></div>
                        <div class="text-center">
                            <a href="#ico" class="btn-b">ICO START <i class="fa fa-time"></i></a>
                        </div>
                    </div>

                </li>

            </ul>
        </div>
        <!-- END REVOLUTION SLIDER -->
    </div>
</div>

<div class="hidden-lg hidden-md background" style="
        background-image: url('/images/nice2.jpg');
        background-position: center;
        background-size: cover;
        background-repeat: no-repeat;
        margin-top: -101px; padding-top: 40px;">
    <div class="buttons-slider">
        <div class="text-center">
            <a href="/login" class="btn-e btn-e-primary">Login <i class="fa fa-user"></i></a> <a
                    href="/register" class="btn-e">Register <i class="fa fa-chain"></i></a>
        </div>

        <div class="countdown-time animated bounceIn" data-date="2018-01-28 00:00:00"
             data-timer="900"></div>
        <div class="text-center">
            <a href="#ico" class="btn-b">ICO START <i class="fa fa-time"></i></a>
        </div>
    </div>
</div>
<!-- END OF SLIDER WRAPPER -->

<div class="clearfix"></div>


<!-- START - Content -->
@yield('content')
<!-- END - Content -->


<div class="clearfix"></div>

<!-- START - Footer -->
<footer>
    <div class="subfooter">

        <div class="container">

            <div class="row">
                <div class="col-md-6">
                    <div class="footer-brand">
                        <p><a href="#">Billionaire Coin</a> &copy; 2017 - All rights reserved.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <p><a href="#">Billionaire coin is a member of FC VIP - CLUB UAE</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END - Footer -->

<!-- START - Back To Top -->
<a href="#" class="toTop">
    <i class="fa fa-chevron-up"></i>
</a>
<!-- END - Back To Top -->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/js/jquery.easing-1.3.min.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="/assets/js/ie10-viewport-bug-workaround.js"></script>

<!-- Scrollspy -->
<script src="/assets/js/scrollspy/scrollspy.js"></script>

<!-- Custom form -->
<script src="/assets/js/form/jcf.js"></script>
<script src="/assets/js/form/jcf.scrollable.js"></script>
<script src="/assets/js/form/jcf.select.js"></script>

<!-- Custom checkbox and radio -->
<script src="/assets/js/checkator/fm.checkator.jquery.js"></script>
<script src="/assets/js/checkator/setting.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="/assets/js/revolution/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/jquery.themepunch.revolution.min.js"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
    (Load Extensions only on Local File Systems !
    The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/assets/js/revolution/revolution.extension.video.min.js"></script>


<!-- Parallax -->
<script src="/assets/js/parallax/jquery.parallax-1.1.3.js"></script>
<script src="/assets/js/parallax/setting.js"></script>

<!-- Owl Carousel -->
<script src="/assets/js/owlcarousel/js/owl.carousel.min.js"></script>
<script src="/assets/js/owlcarousel/js/setting.js"></script>

<!-- PrettyPhoto -->
<script src="/assets/js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
<script src="/assets/js/prettyPhoto/js/setting.js"></script>

<!-- Masonry -->
<script src="/assets/js/masonry/js/masonry.min.js"></script>
<script src="/assets/js/masonry/js/masonry.filter.js"></script>
<script src="/assets/js/masonry/js/setting.js"></script>

<!-- Ticker -->
<script src="/assets/js/ticker/js/ticker.js"></script>


<!-- Nicescroll -->
<script id="nicescrooll" src="/assets/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/assets/js/nicescroll/settings.js"></script>


<script src="/assets/js/jquery.vegas.min.js"></script>
<script src="/assets/js/TimeCircles.js"></script>


<!-- Custom javaScript for this theme -->

<script src="/js/slider.js"></script>
<script src="/js/custom.js"></script>
<script src="/js/counter.js"></script>
<script src="/js/countdown.whitepaper.js"></script>


@yield('scripts')
</body>
</html>
