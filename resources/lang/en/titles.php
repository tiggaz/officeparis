<?php

return [

    'app'           => 'Coins',
    'app2'          => 'Coins',
    'home'          => 'Home',
    'login'         => 'Login',
    'logout'        => 'Logout',
    'register'      => 'Register',
    'resetPword'    => 'Reset Password',
    'toggleNav'     => 'Toggle Navigation',
    'profile'       => 'Settings',
    'editProfile'   => 'Edit Profile',
    'createProfile' => 'Create Profile',

    'activation' => 'Registration Started  | Activation Required',
    'exceeded'   => 'Activation Error',

    'editProfile'    => 'Edit Profile',
    'createProfile'  => 'Create Profile',
    'adminUserList'  => 'Users Administration',
    'adminEditUsers' => 'Edit Users',
    'adminNewUser'   => 'Create New User',

    'adminThemesList' => 'Themes',
    'adminThemesAdd'  => 'Add New Theme',

    'adminLogs'   => 'Log Files',
    'adminPHP'    => 'PHP Information',
    'adminRoutes' => 'Routing Details',

];
